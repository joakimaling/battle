CPPFLAGS = -MMD -MP
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11
LDFLAGS = $(shell pkg-config --libs allegro{,_acodec,_dialog,_image,_ttf}-5)
sources = $(shell find . -name '*.cpp')
objects = $(sources:.cpp=.o)
depends = $(objects:.o=.d)
target = battle

.PHONY: clean run

# Link objects into target
$(target): $(objects)
	$(CXX) $(LDFLAGS) -o $@ $^

# Include dependencies
-include $(depends)

# Compile source code & create dependencies
%.o: %.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -c -o $@ $<

# Remove dependencies, objects & target
clean:
	$(RM) $(depends) $(objects) $(target)

# Run target
run: $(target)
	./$(target)
