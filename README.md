# battle

> An isometric real-time strategy game

[![Licence][licence-badge]][licence-url]

Battle's a simple isometrically projected [real-time strategy][wiki-url] game
inspired by many famous titles in the genre.

## Installation

:bulb: Requires [Allegro 5](https://liballeg.org/download.html).

Clone the project like this:

```sh
git clone https://gitlab.com/joakimaling/battle.git
```

## Usage

Run this code to compile and run the game:

```sh
make && ./battle
```

## Licence

Released under GNU-3.0. See [LICENSE][licence-url] for more.

Coded with :heart: by [joakimaling][user-url].

[licence-badge]: https://badgen.net/gitlab/license/joakimaling/battle
[licence-url]: LICENSE
[wiki-url]: https://en.wikipedia.org/wiki/Real-time_strategy
[user-url]: https://gitlab.com/joakimaling
