#include <allegro5/allegro_native_dialog.h>
#include "core/engine.hpp"

/**
 * Battle v0.1.0
 */
int main() {
	try {
		battle::Engine engine;
		engine.start();
	}
	catch (const std::runtime_error& e) {
		al_show_native_message_box(NULL, "Error", "Error", e.what(), NULL, 2);
	}

	return 0;
}
