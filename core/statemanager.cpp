#include "statemanager.hpp"

namespace battle {
	/**
	 * Deletes each state in the stack to free up memory upon termination of the
	 * game.
	 */
	StateManager::~StateManager() {
		while (!states.empty()) {
			pop();
		}
	}

	/**
	 * Runs the draw method of the active state.
	 */
	void StateManager::draw() {
		states.top()->draw();
	}

	/**
	 * Runs the active state's method which handles events such as keyboard and
	 * mouse inputs.
	 *
	 * @param event A struct containing information about the event
	 */
	void StateManager::handleEvent(ALLEGRO_EVENT event) {
		states.top()->handleEvent(event);
	}

	/**
	 * Removes a state from the stack effectively switching the state of the
	 * game to the previous one.
	 */
	void StateManager::pop() {
		delete states.top();
		states.pop();
	}

	/**
	 * Pushes a new state onto the stack effectively switching the state of the
	 * game to this one.
	 *
	 * @param state A pointer to a class inheriting from the state class
	 */
	void StateManager::push(State *state) {
		states.push(state);
	}

	/**
	 * Runs the tick method of the active state.
	 */
	void StateManager::tick() {
		states.top()->tick();
	}
}
