#include "engine.hpp"

namespace battle {
	/**
	 * Creates the display onto which to draw the graphics, the event queue, and
	 * the timer which will keep the game running at the same speed across every
	 * system. Initialise the keyboard, the mouse, fonts for in-game text, and
	 * the first game state.
	 */
	Engine::Engine() {
		if (!al_init()) {
			throw std::runtime_error("Couldn't initialise Allegro");
		}

		if (!(display = al_create_display(width, height))) {
			throw std::runtime_error("Couldn't create the display");
		}

		if (!(eventQueue = al_create_event_queue())) {
			throw std::runtime_error("Couldn't create the event queue");
		}

		if (!(timer = al_create_timer(1.0 / 60.0))) {
			throw std::runtime_error("Couldn't create the timer");
		}

		if (!al_install_keyboard()) {
			throw std::runtime_error("Couldn't install the keyboard");
		}

		if (!al_install_mouse()) {
			throw std::runtime_error("Couldn't install the mouse");
		}

		ALLEGRO_MONITOR_INFO monitor;
		al_get_monitor_info(0, &monitor);

		// Calculate the coordinates for the upper left corner which will centre
		// the window
		const int centreHeight = (monitor.y2 - monitor.y1 - height) / 2;
		const int centreWidth = (monitor.x2 - monitor.x1 - width) / 2;

		al_set_window_position(display, centreWidth, centreHeight);
		al_set_window_title(display, (title + " v" + version).c_str());

		if (!al_init_image_addon()) {
			throw std::runtime_error("Couldn't initialise the image addon");
		}

		al_set_display_icon(display, al_load_bitmap("assets/icon.png"));

		al_register_event_source(eventQueue, al_get_display_event_source(display));
		al_register_event_source(eventQueue, al_get_timer_event_source(timer));
		al_register_event_source(eventQueue, al_get_keyboard_event_source());
		al_register_event_source(eventQueue, al_get_mouse_event_source());

		if (!al_init_font_addon()) {
			throw std::runtime_error("Couldn't initialise the font addon");
		}

		if (!al_init_ttf_addon()) {
			throw std::runtime_error("Couldn't initialise TrueType font addon");
		}

		if (!(font = al_load_font("assets/arial.ttf", 18, 0))) {
			throw std::runtime_error("Couldn't load the font");
		}

		stateManager.push(new Game());
		al_start_timer(timer);
	}

	/**
	 * Destroys the display, event queue, timer and fonts used by the engine to
	 * free up memory.
	 */
	Engine::~Engine() {
		al_destroy_event_queue(eventQueue);
		al_destroy_display(display);
		al_destroy_timer(timer);
		al_destroy_font(font);
	}

	/**
	 * Prints debug information about the game state, keyboard presses and the
	 * mouse position.
	 */
	void Engine::debug() {
		ALLEGRO_MOUSE_STATE mouse;

		al_get_mouse_state(&mouse);

		al_draw_multiline_textf(
			font,
			al_map_rgb(255, 255, 255),
			10,
			10,
			150,
			20,
			0,
			"Camera: %d, %d\nMouse: %d, %d\nFrames: %i\n",
			0,
			0,
			mouse.x,
			mouse.y,
			0
		);
	}

	/**
	 * Draws everything coming from the state manager to the screen and flips
	 * the buffers. It also draws debug information on top, if activated.
	 */
	void Engine::draw() {
		al_clear_to_color(al_map_rgb(0, 0, 0));
		stateManager.draw();
		debug();
		al_flip_display();
	}

	/**
	 * Runs the game loop. Updates the logic and draws eveything to the screen
	 * by calling the engine's tick and draw methods.
	 */
	void Engine::run() {
		ALLEGRO_EVENT event;
		bool redraw = false;

		while (isRunning) {
			al_wait_for_event(eventQueue, &event);

			switch (event.type) {
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					isRunning = false;
					break;
				case ALLEGRO_EVENT_DISPLAY_SWITCH_IN:
					isHidden = false;
					break;
				case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:
					isHidden = true;
					break;
				case ALLEGRO_EVENT_KEY_DOWN:
				case ALLEGRO_EVENT_KEY_UP:
					stateManager.handleEvent(event);
					break;
				case ALLEGRO_EVENT_TIMER:
					if (!isHidden) {
						redraw = true;
						tick();
					}
			}

			if (redraw && al_is_event_queue_empty(eventQueue)) {
				redraw = false;
				draw();
			}
		}
	}

	/**
	 * Starts the engine and puts it in the run loop.
	 */
	void Engine::start() {
		isRunning = true;
		run();
	}

	/**
	 * Stops the engine by exiting the run loop.
	 */
	void Engine::stop() {
		isRunning = false;
	}

	/**
	 * Does all the engine's logic and calculations.
	 */
	void Engine::tick() {
		stateManager.tick();
	}
}
