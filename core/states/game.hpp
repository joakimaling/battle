#pragma once

#include <allegro5/allegro.h>
#include "../state.hpp"
#include "../world.hpp"

namespace battle {
	class Game: public State {
		private:
			World world;

			int xOffset = 0, yOffset = 0;

			enum { DOWN, LEFT, RIGHT, UP };
			bool keys[4] = { false, false, false, false };

			int velocity = 3;

		public:
			Game();
			~Game() {}

			virtual void draw();
			virtual void handleEvent(ALLEGRO_EVENT);
			virtual void tick();
	};
}
