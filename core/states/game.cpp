#include "game.hpp"

namespace battle {
	/**
	 * Sets up a game by loading the selected world from a file.
	 */
	Game::Game() {
		Assets::get().loadSheets();
		world.load("assets/world1.txt");
	}

	/**
	 * Draws the world, structures, units, map, and HUD to the screen.
	 */
	void Game::draw() {
		world.draw(xOffset, yOffset);
	}

	/**
	 * Handles events from the keyboard and mouse such as scrolling the world,
	 * moving units and selecting structures to build.
	 *
	 * @param event A struct containing information about fired event
	 */
	void Game::handleEvent(ALLEGRO_EVENT event) {
		if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (event.keyboard.keycode) {
				case ALLEGRO_KEY_DOWN:
					keys[DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT:
					keys[LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					keys[RIGHT] = true;
					break;
				case ALLEGRO_KEY_UP:
					keys[UP] = true;
			}
		}
		else if (event.type == ALLEGRO_EVENT_KEY_UP) {
			switch (event.keyboard.keycode) {
				case ALLEGRO_KEY_DOWN:
					keys[DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT:
					keys[LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					keys[RIGHT] = false;
					break;
				case ALLEGRO_KEY_UP:
					keys[UP] = false;
			}
		}
	}

	/**
	 * Handles the logic of the game such as calculating time for constructions,
	 * reserching upgrades and victory conditions.
	 */
	void Game::tick() {
		yOffset -= velocity * keys[DOWN];
		xOffset += velocity * keys[LEFT];
		xOffset -= velocity * keys[RIGHT];
		yOffset += velocity * keys[UP];
	}
}
