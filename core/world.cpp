#include "world.hpp"

namespace battle {
	/**
	 * Draws the world to the screen using the current tile sheet.
	 */
	void World::draw(int xOffset, int yOffset) {
		for (int y = 0; y < height; y++) {
			for (int x = width; x >= 0; x--) {
				al_draw_bitmap_region(
					sheet,
					tiles[x + y * width] * tileWidth,
					tileHeight,
					tileWidth,
					tileHeight,
					(x + y) * (tileWidth / 2) + xOffset,
					(x - y) * (tileHeight / 2) + yOffset,
					0
				);
			}
		}
	}

	/**
	 * Loads a file describing the layout of the world into the tiles vector and
	 * the tile sheet used when drawing it.
	 *
	 * @param filename Name of file containing world data
	 */
	void World::load(const std::string& filename) {
		std::ifstream file(filename);
		int type;

		if (file.is_open()) {
			file >> width >> height >> biome;

			sheet = Assets::get().getSheet(biome);

			while (!file.eof()) {
				file >> type;
				tiles.push_back(type);
			}

			file.close();
		}
	}
}
