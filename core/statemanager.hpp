#pragma once

#include <allegro5/allegro.h>
#include <stack>
#include "state.hpp"

namespace battle {
	class StateManager {
		private:
			std::stack<State*> states;

		public:
			StateManager() {}
			~StateManager();

			void draw();
			void handleEvent(ALLEGRO_EVENT);
			void pop();
			void push(State*);
			void tick();
	};
}
