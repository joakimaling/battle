#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>
#include <stdexcept>
#include <string>
#include "statemanager.hpp"
#include "states/game.hpp"

namespace battle {
	class Engine {
		private:
			ALLEGRO_DISPLAY *display;
			ALLEGRO_FONT *font;
			ALLEGRO_EVENT_QUEUE *eventQueue;
			ALLEGRO_TIMER *timer;

			const std::string title = "Battle";
			const std::string version = "0.1.0";

			const int width = 1280;
			const int height = width / 16 * 9;

			StateManager stateManager;
			bool isHidden = false;
			bool isRunning;

			void debug();
			void draw();
			void run();
			void tick();

		public:
			Engine();
			~Engine();

			void start();
			void stop();
	};
}
