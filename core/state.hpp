#pragma once

#include <allegro5/allegro.h>

namespace battle {
	class State {
		public:
			State() {}
			virtual ~State() {}

			virtual void draw() = 0;
			virtual void handleEvent(ALLEGRO_EVENT) = 0;
			virtual void tick() = 0;
	};
}
