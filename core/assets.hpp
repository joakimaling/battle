#pragma once

#include <allegro5/allegro.h>
#include <map>
#include <stdexcept>
#include <string>

namespace battle {
	class Assets {
		private:
			std::map<std::string, ALLEGRO_BITMAP*> sheets;
			static Assets assets;

			Assets() {}
			~Assets() {}

		public:
			Assets(Assets const&) = delete;
			void operator=(Assets const&) = delete;

			static Assets& get() {
				return assets;
			}

			ALLEGRO_BITMAP* getSheet(const std::string);
			void loadSheets();
	};
}
