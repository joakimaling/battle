#pragma once

#include <fstream>
#include <stdexcept>
#include <string>
#include <vector>
#include "assets.hpp"

namespace battle {
	class World {
		private:
			ALLEGRO_BITMAP *sheet = nullptr;
			std::vector<int> tiles;
			std::string biome;

			int height, width;
			int tileHeight = 27;
			int tileWidth = 54;
		public:
			World() {}
			~World() {}

			void draw(int, int);
			void load(const std::string&);
	};
}
