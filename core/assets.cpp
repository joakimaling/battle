#include "assets.hpp"

namespace battle {
	/**
	 * Returns a sprite sheet with given name.
	 *
	 * @param name Name of the sprite sheet
	 *
	 * @return Requested sprite sheet
	 */
	ALLEGRO_BITMAP* Assets::getSheet(const std::string name) {
		return sheets[name];
	}

	/**
	 * Loads all sprite sheets into a map with given name as key. These will be
	 * accessible via {@see getSheet}.
	 *
	 * @throws
	 */
	void Assets::loadSheets() {
		// Loads an image file into the sheets map paired with given name as key
		auto load = [&](const std::string& filename, const std::string& name) {
			ALLEGRO_BITMAP *sheet;

			if (!(sheet = al_load_bitmap(filename.c_str()))) {
				throw std::runtime_error("Couldn't load '" + name + "'");
			}

			al_convert_mask_to_alpha(sheet, al_map_rgb(255, 0, 255));
			sheets[name] = sheet;
		};

		load("assets/tile_sheet.bmp", "desert");
	}
}
